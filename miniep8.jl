function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

dict_number = Dict("2" => 2, "3" => 3, "4" => 4, "5" => 5, "6" => 6, "7" => 7, "8" => 8, "9" => 9, "10" => 10, "J" => 11, "Q" => 12, "K" => 13, "A" => 14)
function compareByValue(x, y)
	xNumber = x[1:length.(x) - 1]
	yNumber = y[1:length.(y) - 1]
	return dict_number[xNumber] < dict_number[yNumber]
end
dict_naipe = Dict('♦' => 1, '♠' => 2, '♥' => 3, '♣' => 4)
function compareByValueAndSuit(x, y)
	xNaipe = x[length.(x)]
	yNaipe = y[length.(y)]
	if (dict_naipe[xNaipe] < dict_naipe[yNaipe])
		return true
	end

	return compareByValue(x, y)
end

function insercao(v)
	tam = length(v)
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v, j, j - 1)
    			else
				break
			end
			j = j - 1
		end
	end
	return v
end

## testes ##
function testeCompareByValueAndSuit()
	if !compareByValueAndSuit("2♠", "A♠")
		println("compareByValue errado")
	end
	if compareByValueAndSuit("K♥", "10♥")
		println("compareByValue errado")
	end
	if !compareByValueAndSuit("10♠", "10♥")
		println("compareByValue errado")
	end
	if !compareByValueAndSuit("A♠", "2♥")
		println("compareByValue errado")
	end
end
testeCompareByValueAndSuit()
